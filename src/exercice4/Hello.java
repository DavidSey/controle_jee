package exercice4;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Hello
 */
@WebServlet("/Hello")
public class Hello extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Hello() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<html>" + "<head><title>Hello</title></head>");

		out.println(
				"<body  bgcolor=\"#ffffff\">"
		        + "<h2>Bienvenue sur mon site !</h2>"
				+ "<img src=\"images/duke.waving.gif\" alt=\"Duke waving\">"
				+ "<h2>Quel est votre nom ?</h2>"
				+ "<form method=\"post\">"
				+ "<input type=\"text\" name=\"username\" size=\"25\">"
				+ "<p></p>" + "<input type=\"submit\" value=\"Submit\">"
				+ "<input type=\"reset\" value=\"Reset\">" + "</form>");

		String username = request.getParameter("username");

		if ((username != null) && (username.length() > 0)) {
			out.println("<h2>Hello, " + username + "!</h2>");
		}

		out.println("</body></html>");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		request.setAttribute("username", username);
		doGet(request, response);
	}

}
