package exercice6;

import java.util.ArrayList;

public class ListeObjets {

  private ArrayList<ObjetAVendre> liste;
  
  public ListeObjets() {
    liste = new ArrayList<ObjetAVendre>();
    liste.add(new ObjetAVendre("livre", 15));
    liste.add(new ObjetAVendre("PC", 500));
    liste.add(new ObjetAVendre("CD", 10));
    liste.add(new ObjetAVendre("Voiture", 6000));
    
  }
  
  @Override
  public String toString() {
    return liste.toString();
  }

  public ArrayList<ObjetAVendre> getListe() {
    return liste;
  }
}