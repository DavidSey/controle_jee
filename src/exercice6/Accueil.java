package exercice6;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Accueil
 */
@WebServlet("/ecommerce/Accueil")
public class Accueil extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Accueil() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	if (request.getSession().getAttribute("username") != null) {
    		response.sendRedirect("/ecommerce/en_stock");
    	}
    	
    	response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        out.println("<html><head><title>Hello</title></head>");
        
        out.println(
				"<body  bgcolor=\"#ffffff\">"
		        + "<h2>Bienvenue sur mon site !</h2>"
				+ "<img src=\"../images/duke.waving.gif\" alt=\"Duke waving\">"
				+ "<h2>Quel est votre nom ?</h2>"
				+ "<form method=\"post\">"
				+ "<input type=\"text\" name=\"username\" size=\"25\">"
				+ "<p></p>" + "<input type=\"submit\" value=\"Submit\">"
				+ "<input type=\"reset\" value=\"Reset\">" + "</form>");

        out.println("</body></html>");
        out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		HttpSession session = request.getSession();
		session.setAttribute("username", username);
		response.sendRedirect("/ecommerce/en_stock");
	}

}
