package exercice6;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Stock
 */
@WebServlet("/ecommerce/en_stock")
public class Stock extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Stock() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ListeObjets listObj = new ListeObjets();
		HttpSession session = request.getSession();
		
		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
		out.print("<html><head><title>Hello</title></head>"
				+"<body  bgcolor=\"#ffffff\">"
				+"<a href=\"ecommerce/panier\">Panier</a>");
		if (session.getAttribute("nbProduits") == null) {
			out.println(" Vous avez 0 produits dans le panier");
		} else {
			out.println(" Vous avez "+session.getAttribute("nbProduits") 
					+ " produits dans le panier");
		}
		
		out.println( "<form method=\"post\">"
				+"<table>");
		for (ObjetAVendre o : listObj.getListe()) {
			out.println(
					"<tr>"
					+"<td>" + o.getDescription() + "</td>"
					+"<td>" + o.getPrix() + "</td>"
					+"<td><input type=\"submit\" value=\"Ajouter\"></td>"
					+ "</tr>");
		}
		
		out.println("</table></form></body></html>");
        out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		int nbProduits = 0;
		if (session.getAttribute("nbProduits") != null) {
			nbProduits = (int)session.getAttribute("nbProduits") + 1;
		}
		session.setAttribute("nbProduits", nbProduits);
		doGet(request, response);
	}

}
