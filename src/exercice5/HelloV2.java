package exercice5;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloV2
 */
@WebServlet({"/exercice5", "/exo5"})
public class HelloV2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloV2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie username = null;
		
		if (request.getCookies() != null) {
			for(Cookie c : request.getCookies()) {
				if (c.getName().equals("username")) {
					username = c;
				}
			}
		}
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<html>" + "<head><title>Hello</title></head>");

		out.println(
				"<body  bgcolor=\"#ffffff\">"
		        + "<h2>Bienvenue sur mon site !</h2>"
				+ "<img src=\"images/duke.waving.gif\" alt=\"Duke waving\">"
				+ "<h2>Quel est votre nom ?</h2>"
				+ "<form method=\"post\">"
				+ "<input type=\"text\" name=\"username\" size=\"25\">"
				+ "<p></p>" + "<input type=\"submit\" value=\"Submit\">"
				+ "<input type=\"reset\" value=\"Reset\">" + "</form>");


		if ((username != null)) {
			out.println("<h2>Hello, " + username.getValue() + "!</h2>");
		}

		out.println("</body></html>");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		Cookie user = new Cookie("username", username);
		user.setMaxAge(60*60*24);
		response.addCookie(user);
		doGet(request, response);
	}

}
